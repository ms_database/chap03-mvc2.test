package com.solo.model.stardao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class SoloDAO {

	Properties prop = new Properties();
	
	public int newMenu(Connection con, String titlename, String subname) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("newMenu");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, titlename);
			pstmt.setString(2, subname);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		
		return 0;
	}

}
