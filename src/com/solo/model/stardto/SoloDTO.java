package com.solo.model.stardto;

import java.util.Date;

public class SoloDTO {
	
	private int empid;
	private String empname;
	private String deptcode;
	private Date date;
	
	
	public SoloDTO() {}
	
	
	public SoloDTO(int empid, String empname, String deptcode, Date date) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.deptcode = deptcode;
		this.date = date;
	}


	public int getEmpid() {
		return empid;
	}


	public void setEmpid(int empid) {
		this.empid = empid;
	}


	public String getEmpname() {
		return empname;
	}


	public void setEmpname(String empname) {
		this.empname = empname;
	}


	public String getDeptcode() {
		return deptcode;
	}


	public void setDeptcode(String deptcode) {
		this.deptcode = deptcode;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "SoloDTO [empid=" + empid + ", empname=" + empname + ", deptcode=" + deptcode + ", date=" + date + "]";
	}
	
	
	
	
	
	

}
